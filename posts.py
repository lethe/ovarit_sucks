#!/usr/bin/env python3

# importing dependencies
import selenium
from selenium import webdriver
from selenium.webdriver.support.select import Select
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
import time

def NUKE_POSTS(username, password, WAIT_TIME = 3):
	options = webdriver.ChromeOptions()
	# don't make a browser window pop up
	options.add_argument('headless')
	# change window size to something reasonable
	options.add_argument('window-size=1200x600')
	# set user-agent to something reasonable
	options.add_argument('user-agent="Mozilla/5.0 (Windows NT 10.0; rv:91.0) Gecko/20100101 Firefox/91.0"')
	# create browser instance
	driver = webdriver.Chrome(chrome_options=options)
	# go directly to Ovarit login page
	driver.get('https://ovarit.com/login')
	# wait for the page to load (useful for slow connections)
	driver.implicitly_wait(WAIT_TIME)
	# cursor selects username field (HTML "input" element with CSS ID "username")
	usernameBox = driver.find_element_by_css_selector('input[id=username]')
	# browser types username
	usernameBox.send_keys(username.strip())
	# cursor selects password field (HTML "input" element with CSS ID "password")
	passwordBox = driver.find_element_by_css_selector('input[id=password]')
	# browser types password
	passwordBox.send_keys(password.strip())
	# browser clicks the button that says "Log in"
	driver.find_element_by_xpath("// button[contains(text(),'Log in')]").click()
	# wait for the page to load
	driver.implicitly_wait(WAIT_TIME)
	# infinite loop until error
	try:
		while(True):
			# go straight to posts page of user
			driver.get('https://ovarit.com/u/' + username + '/posts')
			# wait a few seconds
			driver.implicitly_wait(WAIT_TIME)
			# delete mod-censored posts from webpage source so that Selenium doesn't shit itself when the post fails to delete
			driver.execute_script("""
				function removeChildren(cssSelector, parentNode){
					var elements = parentNode.querySelectorAll(cssSelector);
					let fragment = document.createDocumentFragment();
					fragment.textContent=' ';
					fragment.firstChild.replaceWith(...elements);
				}
				removeChildren('.deletedpost',document.body);
			""")
			# click first found "comments" button
			driver.find_element(By.CLASS_NAME, "comments").click()
			# wait a few seconds for page to load
			driver.implicitly_wait(WAIT_TIME)
			# click "delete" and "yes" buttons
			#
			# And a big hearty fuck-you to whatever developer for Ovarit
			# decided to make the "yes" button so damn hard to select.
			# I spent two hours trying every combination of "select by
			# inner text", "select by parent element", and "select by
			# "index of anchor tag on page".
			# Finally what worked was forcing the Chromium instance
			# to wait a few seconds before killing itself to give
			# the time for whatever JavaScript controls "delete this
			# post" to execute.
			driver.execute_script("""
				for (const a of document.querySelectorAll("a")) {
					if (a.textContent.includes("delete")) {
						a.click();
					}
				}
			""")
			driver.execute_script("""
				for (const a of document.querySelectorAll("a")) {
					if (a.textContent.includes("yes")) {
						a.click();
					}
				}
			""")
			time.sleep(5)
			print("Post deleted")
	except :
		print("Are there any posts left? The script can't seem to find any.")

NUKE_POSTS("UsernameHere", "A long password with spaces in it to prove the script still works if you have spaces in your password")
