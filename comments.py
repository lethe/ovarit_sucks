#!/usr/bin/env python3

# importing dependencies
import selenium
from selenium import webdriver
from selenium.webdriver.support.select import Select
from selenium.webdriver.common.by import By

def NUKE_COMMENTS(username, password, WAIT_TIME = 3):
	options = webdriver.ChromeOptions()
	# don't make a browser window pop up
	options.add_argument('headless')
	# change window size to something reasonable
	options.add_argument('window-size=800x600')
	# set user-agent to something reasonable
	options.add_argument('user-agent="Mozilla/5.0 (Windows NT 10.0; rv:91.0) Gecko/20100101 Firefox/91.0"')
	# create browser instance
	driver = webdriver.Chrome(chrome_options=options)
	# go directly to Ovarit login page
	driver.get('https://ovarit.com/login')
	# wait for the page to load (useful for slow connections)
	driver.implicitly_wait(WAIT_TIME)
	# cursor selects username field (HTML "input" element with CSS ID "username")
	usernameBox = driver.find_element_by_css_selector('input[id=username]')
	# browser types username
	usernameBox.send_keys(username.strip())
	# cursor selects password field (HTML "input" element with CSS ID "password")
	passwordBox = driver.find_element_by_css_selector('input[id=password]')
	# browser types password
	passwordBox.send_keys(password.strip())
	# browser clicks the button that says "Log in"
	driver.find_element_by_xpath("// button[contains(text(),'Log in')]").click()
	# wait for the page to load
	driver.implicitly_wait(WAIT_TIME)
	# infinite loop until error
	try:
		while(True):
			# go straight to comments page of user
			driver.get('https://ovarit.com/u/' + username + '/comments')
			# delete mod-censored comments from webpage source so that Selenium doesn't shit itself when the comment fails to delete
			driver.execute_script("""
				function removeChildren(cssSelector, parentNode){
					var elements = parentNode.querySelectorAll(cssSelector);
					let fragment = document.createDocumentFragment();
					fragment.textContent=' ';
					fragment.firstChild.replaceWith(...elements);
				}
				removeChildren('.deletedcomment',document.body);
			""")
			# click first found "delete" button
			driver.find_element(By.CLASS_NAME, "delete-comment").click()
			# click "yes" confirmation
			driver.find_element_by_link_text("yes").click()
			print("Comment deleted")
			# wait a few seconds
			driver.implicitly_wait(WAIT_TIME)
	except selenium.common.exceptions.JavascriptException:
		Pissbaby()
	except selenium.common.exceptions.NoSuchElementException:
		Pissbaby()

def Pissbaby():
	print("Oh boy, Python threw an error!")
	print("Looks like the script went a little too fast and Ovarit threw a tantrum.")
	print("Your progress until now has been saved. (Un-saved?) The error means it was not able to delete any more comments.")
	print("Usually this means Selenium (what controls the Headless Chromium instance) tripped over a comment the mods censored and forcibly-deleted-but-not-actually.")
	print("If you are sure there are more comments to delete, wait about ten seconds and then run the script again.")

NUKE_COMMENTS("UsernameHere", "A long password with spaces in it to prove the script still works if you have spaces in your password")
