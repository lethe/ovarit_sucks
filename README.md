# Ovarit mass post/comment deleter

So you've gotten sick of the ableism / racism / caping for harassment forums online endemic to Ovarit and have decided to go spend your time on a different forum where people talk about a wider range of topics than "haha, look at this ugly trans-identified male" or "yet another trans rights activist gets busted for pedophilia". Or maybe Kiwi Farms is after you for the crime of being a woman on the Internet and you need to Delete Fucking Everything ASAP for your personal safety. (Simping for Joshua Moon because of "muh freeze peach" isn't going to save you from his hate mob when they decide to turn their sights on you.) Or maybe you just don't like the moral puritanism of the mods of o/Radfemmery. One problem: deleting your account *doesn't* automatically delete all your posts and comments. That's where these two scripts come in: it runs an instance of Google Chrome (or another Chromium-compatible browser) in the background to simulate a person manually clicking "delete" on all your posts and comments, but much faster (and with far less hand/eye strain) than doing it by hand.

If Ovarit ever changes its code in the future, I have both of these scripts well-commented so, even if you're not familiar with Python, it should be easy to fix the scripts to make them work again. As obviously I have deleted my account, I cannot maintain the scripts myself unless there's a bug you want fixed and you're willing to donate your account (that you've already set your mind on deleting) to me. Don't offer me invite codes because I'm not putting in the effort of making test data; I've got better things to do with my time.

## Dependencies

- A browser compatible with [Selenium](https://pypi.org/project/selenium/). This script is set to use Chrome (or Chromium, Iridium, etc.) by default, but it should be piss-easy to change it to use Firefox instead by reading the aforementioned Selenium documentation. Assuming you know Python, of course.
- [Selenium](https://pypi.org/project/selenium/), the program that lets Python automate stuff in a browser. FreeBSD users can just run `pkg -4 install py39-selenium` to install. Linux users know how to work a package manager. (I unfortunately can't give more specific instructions because there are so many damn distros in the world and they all use different naming conventions for their Python modules.) Windows users can [read this page](https://selenium-python.readthedocs.io/installation.html), I guess. MacOS users can go install a better operating system in VirtualBox and then follow the directions from there.

## Running

Open both `comments.py` and `posts.py` in your favorite text editor. Change the last line to have your actual username and password.

UNIX (Linux, \*BSD, Haiku) users: run `python3 /wherever/you/downloaded/this/comments.py` or `python3 /wherever/you/downloaded/this/posts.py` in a terminal.

If that doesn't work, or you're on Windows, [read this](https://realpython.com/run-python-scripts/).

## License

[MIT](https://opensource.org/licenses/MIT). TL;DR: I don't care what you do with this, but I'm not legally responsible for whatever you do.
